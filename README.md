# Environment

This repository contains two main things:

- **Vagrantfile** that allows you to set up local application environment on virtual machines for development/testing purpose
- **Ansible playbooks** that can be used for application deployment on any environment (local/dev/prod)

## Setting up local environment for development

1. Install [VirtualBox](https://www.virtualbox.org/) and [Vagrant](https://www.vagrantup.com/)
2. Clone this repository to your machine
3. Move to the repository folder and create required folders: `mkdir front-end && mkdir back-end`
4. Run `vagrant up`

After this you'll have several running virtual machines:

IP            | Name
--------------|-----------
192.168.33.10 | nginx
192.168.33.11 | node
192.168.33.12 | postgresql
192.168.33.13 | redis

Application code will be available in `./back-end` `./front-end` folders, that are synchronized with corresponded folders on the `node` virtual machine.

More info about using Vagrant: [https://www.vagrantup.com/docs/index.html](https://www.vagrantup.com/docs/index.html)

## Using Ansible playbooks

### Available playbooks:

All playbooks are placed to `./playbooks` directory and grouped in subdirectories by purpose.

- **app/install-app.yml**:
- **app/setup-app.yml**:
- **mongodb/mongodb-vagrant.yml**: install and setup MongoDB on Vagrant VM
- **mongodb/mongodb.yml**: install and setup MongoDB
- **nginx/nginx-vagrant.yml**: install and setup Nginx on Vagrant VM
- **nginx/nginx.yml**: install and setup Nginx
- **node/node-vagrant.yml**: install and setup Node.js and Yarn on Vagrant VM
- **node/node.yml**: install and setup Node.js and Yarn
- **postgresql/postgresql-vagrant.yml**: install and setup PostgreSQL on Vagrant VM
- **postgresql/postgresql.yml**: install and setup PostgreSQL
- **redis/redis-vagrant.yml**: install and setup Redis on Vagrant VM
- **redis/redis.yml**: install and setup Redis
- **setup-environment**: wraps all "setup" playbooks in one in order to configure environment from scratch with one command


ansible-galaxy install -r requirements.yml
